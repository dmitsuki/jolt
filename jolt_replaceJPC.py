import os

def replace_jpc_with_jolt(file_path):
    try:
        # Read the content of the C++ file
        with open(file_path, 'r') as file:
            content = file.read()

        # Replace all instances of 'JPC_' with 'JOLT_'
        modified_content = content.replace('JOLT_API', '')

        # Write the modified content back to the file
        with open(file_path, 'w') as file:
            file.write(modified_content)

        print(f"Successfully replaced 'JPC_' with 'JOLT_' in {file_path}")

    except Exception as e:
        print(f"Error processing file {file_path}: {e}")

def process_directory(directory):
    # Iterate through all files in the directory
        file_path = os.path.join(directory, "jolt_bind.cpp")
        #if os.path.isfile(file_path) and file_path.endswith('.h'):
        replace_jpc_with_jolt(file_path)

# Replace 'JPC_' with 'JOLT_' in all .cpp files in the current directory and its subdirectories
process_directory(os.getcwd())
